# Web Game of Life

This is a WebAssembly version of game of life written in Rust.
It is based off of [this tutorial](https://rustwasm.github.io/docs/book/game-of-life/introduction.html).

## Dependencies

You need [Rust](https://www.rust-lang.org/tools/install), [wasm-pack](https://rustwasm.github.io/wasm-pack/installer/) and [npm](https://www.npmjs.com/get-npm).

## 🚴 Usage

Clone the repository in the root directory:
```sh
wasm-pack build
cd www
npm install
export NODE_OPTIONS=--openssl-legacy-provider
npm run start
```

Then visit http://localhost:8080/ to view the website.

## Testing

To run the unit tests use:
```sh
wasm-pack test --firefox --headless
```

## 🔋 Batteries Included

* [`wasm-bindgen`](https://github.com/rustwasm/wasm-bindgen) for communicating
  between WebAssembly and JavaScript.
* [`console_error_panic_hook`](https://github.com/rustwasm/console_error_panic_hook)
  for logging panic messages to the developer console.
* [`wee_alloc`](https://github.com/rustwasm/wee_alloc), an allocator optimized
  for small code size.
* `LICENSE-APACHE` and `LICENSE-MIT`: most Rust projects are licensed this way, so these are included for you

## License

Licensed under either of

* Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
* MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.
