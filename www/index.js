// Import Universe, Cell and InitMode from Rust wasm
import { Universe, Cell, InitMode } from "wasm-game-of-life";

// Import the WebAssembly memory at the top of the file.
import { memory } from "wasm-game-of-life/wasm_game_of_life_bg";

const CELL_SIZE = 5; // px
const GRID_COLOR = "#CCCCCC";
const DEAD_COLOR = "#FFFFFF";
const ALIVE_COLOR = "#000000";

// Construct the universe, and get its width and height.
let universe = Universe.new(InitMode.Zero);
const width  = universe.width();
const height = universe.height();

// Give the canvas room for all of our cells and a 1px border
// around each of them.
const canvas  = document.getElementById("game-of-life-canvas");
canvas.height = (CELL_SIZE + 1) * height + 1;
canvas.width  = (CELL_SIZE + 1) * width + 1;
const ctx     = canvas.getContext('2d');

let animationId = null;
let ticksPerStep = 1;

////////////////////////
// Rendering funtions //
////////////////////////

const renderLoop = () => {
  fps.render();

  // Used to enter debugger after every step:
  //debugger;
  for (let i = 0; i < ticksPerStep; i++) {
    universe.tick();
  }

  drawGrid();
  drawCells();

  // Necessary for play/pause functionality
  animationId = requestAnimationFrame(renderLoop);
};

const drawGrid = () => {
  ctx.beginPath();
  ctx.strokeStyle = GRID_COLOR;

  // Vertical lines.
  for (let i = 0; i <= width; i++) {
    ctx.moveTo(i * (CELL_SIZE + 1) + 1, 0);
    ctx.lineTo(i * (CELL_SIZE + 1) + 1, (CELL_SIZE + 1) * height + 1);
  }

  // Horizontal lines.
  for (let j = 0; j <= height; j++) {
    ctx.moveTo(0,                           j * (CELL_SIZE + 1) + 1);
    ctx.lineTo((CELL_SIZE + 1) * width + 1, j * (CELL_SIZE + 1) + 1);
  }

  ctx.stroke();
};


const getIndex = (row, column) => {
  return row * width + column;
};

const drawCells = () => {
  const cellsPtr = universe.cells();
  const cells = new Uint8Array(memory.buffer, cellsPtr, width * height);

  ctx.beginPath();

  // Alive cells.
  ctx.fillStyle = ALIVE_COLOR;
  for (let row = 0; row < height; row++) {
    for (let col = 0; col < width; col++) {
      const idx = getIndex(row, col);
      if (cells[idx] !== Cell.Alive) {
        continue;
      }

      ctx.fillRect(
        col * (CELL_SIZE + 1) + 1,
        row * (CELL_SIZE + 1) + 1,
        CELL_SIZE,
        CELL_SIZE
      );
    }
  }

  // Dead cells.
  ctx.fillStyle = DEAD_COLOR;
  for (let row = 0; row < height; row++) {
    for (let col = 0; col < width; col++) {
      const idx = getIndex(row, col);
      if (cells[idx] !== Cell.Dead) {
        continue;
      }

      ctx.fillRect(
        col * (CELL_SIZE + 1) + 1,
        row * (CELL_SIZE + 1) + 1,
        CELL_SIZE,
        CELL_SIZE
      );
    }
  }

  ctx.stroke();
};

//////////////////////////////
// Play-pause functionality //
//////////////////////////////

const isPaused = () => {
  return animationId === null;
};

const playPauseButton = document.getElementById("play-pause");

const play = () => {
  playPauseButton.textContent = "⏸";
  renderLoop();
};

const pause = () => {
  playPauseButton.textContent = "▶";
  cancelAnimationFrame(animationId);
  animationId = null;
};

playPauseButton.addEventListener("click", event => {
  if (isPaused()) {
    play();
  } else {
    pause();
  }
});

////////////////////
// Toggling cells //
////////////////////

canvas.addEventListener("click", event => {
  const boundingRect = canvas.getBoundingClientRect();

  const scaleX = canvas.width / boundingRect.width;
  const scaleY = canvas.height / boundingRect.height;

  const canvasLeft = (event.clientX - boundingRect.left) * scaleX;
  const canvasTop = (event.clientY - boundingRect.top) * scaleY;

  let row = Math.min(Math.floor(canvasTop / (CELL_SIZE + 1)), height - 1);
  let col = Math.min(Math.floor(canvasLeft / (CELL_SIZE + 1)), width - 1);

  if (event.ctrlKey) {
    if (row === 0)          row += 1;
    if (col === 0)          col += 1;
    if (row === height - 1) row -= 1;
    if (col === width - 1)  col -= 1;
    // X__
    // _XX
    // XX_
    universe.toggle_cell(row - 1, col - 1);
    universe.toggle_cell(row    , col    );
    universe.toggle_cell(row    , col + 1);
    universe.toggle_cell(row + 1, col - 1);
    universe.toggle_cell(row + 1, col    );
  } else if (event.shiftKey) {
    // 6543210123456
    // __XXX___XXX__
    // _____________
    // X____X_X____X
    // X____X_X____X
    // X____X_X____X
    // __XXX___XXX__
    // _____________
    // __XXX___XXX__
    // X____X_X____X
    // X____X_X____X
    // X____X_X____X
    // _____________
    // __XXX___XXX__

    if (row < 7)          row = 7;
    if (col < 7)          col = 7;
    if (row > height - 8) row = height - 8;
    if (col > width - 8)  col = width - 8;

    // Row -6
    universe.toggle_cell(row - 6, col - 4);
    universe.toggle_cell(row - 6, col - 3);
    universe.toggle_cell(row - 6, col - 2);
    universe.toggle_cell(row - 6, col + 2);
    universe.toggle_cell(row - 6, col + 3);
    universe.toggle_cell(row - 6, col + 4);
    // Row -5
    // Row -4
    universe.toggle_cell(row - 4, col - 6);
    universe.toggle_cell(row - 4, col - 1);
    universe.toggle_cell(row - 4, col + 1);
    universe.toggle_cell(row - 4, col + 6);
    // Row -3
    universe.toggle_cell(row - 3, col - 6);
    universe.toggle_cell(row - 3, col - 1);
    universe.toggle_cell(row - 3, col + 1);
    universe.toggle_cell(row - 3, col + 6);
    // Row -2
    universe.toggle_cell(row - 2, col - 6);
    universe.toggle_cell(row - 2, col - 1);
    universe.toggle_cell(row - 2, col + 1);
    universe.toggle_cell(row - 2, col + 6);
    // Row -1
    universe.toggle_cell(row - 1, col - 4);
    universe.toggle_cell(row - 1, col - 3);
    universe.toggle_cell(row - 1, col - 2);
    universe.toggle_cell(row - 1, col + 2);
    universe.toggle_cell(row - 1, col + 3);
    universe.toggle_cell(row - 1, col + 4);
    // Row 0
    // Row 1
    universe.toggle_cell(row + 1, col - 4);
    universe.toggle_cell(row + 1, col - 3);
    universe.toggle_cell(row + 1, col - 2);
    universe.toggle_cell(row + 1, col + 2);
    universe.toggle_cell(row + 1, col + 3);
    universe.toggle_cell(row + 1, col + 4);
    // Row 2
    universe.toggle_cell(row + 2, col - 6);
    universe.toggle_cell(row + 2, col - 1);
    universe.toggle_cell(row + 2, col + 1);
    universe.toggle_cell(row + 2, col + 6);
    // Row 3
    universe.toggle_cell(row + 3, col - 6);
    universe.toggle_cell(row + 3, col - 1);
    universe.toggle_cell(row + 3, col + 1);
    universe.toggle_cell(row + 3, col + 6);
    // Row 4
    universe.toggle_cell(row + 4, col - 6);
    universe.toggle_cell(row + 4, col - 1);
    universe.toggle_cell(row + 4, col + 1);
    universe.toggle_cell(row + 4, col + 6);
    // Row 5
    // Row 6
    universe.toggle_cell(row + 6, col - 4);
    universe.toggle_cell(row + 6, col - 3);
    universe.toggle_cell(row + 6, col - 2);
    universe.toggle_cell(row + 6, col + 2);
    universe.toggle_cell(row + 6, col + 3);
    universe.toggle_cell(row + 6, col + 4);
  } else {
    universe.toggle_cell(row, col);
  }

  drawGrid();
  drawCells();
});

/////////////////////////
// Tick range listener //
/////////////////////////

const range =  document.getElementById("tick-range");

range.addEventListener("input", event => {
  ticksPerStep = event.target.value;
});

// Universe generation //
const randomize = () => {
  universe = Universe.new(InitMode.Random);
  drawGrid();
  drawCells();
};

const zeroize = () => {
  universe = Universe.new(InitMode.Zero);
  drawGrid();
  drawCells();
};

const randomizeButton = document.getElementById("randomize");
const killButton = document.getElementById("zeroize");

randomizeButton.addEventListener("click", event => {
  randomize();
});

killButton.addEventListener("click", event => {
  zeroize();
});

////////////////////////////
// Performance monitoring //
////////////////////////////

const fps = new class {
  constructor() {
    this.fps = document.getElementById("fps");
    this.frames = [];
    this.lastFrameTimeStamp = performance.now();
  }

  render() {
    // Convert the delta time since the last frame render into a measure
    // of frames per second.
    const now = performance.now();
    const delta = now - this.lastFrameTimeStamp;
    this.lastFrameTimeStamp = now;
    const fps = 1 / delta * 1000;

    // Save only the latest 100 timings.
    this.frames.push(fps);
    if (this.frames.length > 100) {
      this.frames.shift();
    }

    // Find the max, min, and mean of our 100 latest timings.
    let min = Infinity;
    let max = -Infinity;
    let sum = 0;
    for (let i = 0; i < this.frames.length; i++) {
      sum += this.frames[i];
      min = Math.min(this.frames[i], min);
      max = Math.max(this.frames[i], max);
    }
    let mean = sum / this.frames.length;

    // Render the statistics.
    this.fps.textContent = `
Frames per Second:
         latest = ${Math.round(fps)}
avg of last 100 = ${Math.round(mean)}
min of last 100 = ${Math.round(min)}
max of last 100 = ${Math.round(max)}
`.trim();
  }
};

//////////////////
// Help message //
//////////////////

const helpButton = document.getElementById("help");
const helpDiv = document.getElementById("help-msg");

let displayHelp = false;

helpButton.addEventListener("click", event => {
  if (displayHelp) {
    helpDiv.innerHTML = "";
  } else {
    helpDiv.innerHTML = "This is an adaptation of <a href=\"https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life\">Conway's Game of Life</a>. " +
      "I have used Rust running in WebAssembly, as well as some Javascript for the glue. " +
      "The implemenation is based on the <a href=\"https://rustwasm.github.io/docs/book/game-of-life/introduction.html\">Rust WebAssembly book</a>. " +
      "The source code for this can be found on <a href=\"https://codeberg.org/marno/web-game-of-life\">Codeberg</a>." +
      "Press shuffle to randomize the canvas, the skull to kill all the cells, the pause/play button to pause/play the simulation, " +
      "adjust the slider to increase the number of ticks per render and the question mark to hide this message. " +
      "You can toggle a cell by clicking on it, add a glider by using ctrl-click and add a pulsar by using shift-click.";
    helpDiv.setAttribute("style", "width:" + canvas.width.toString() + "px");
  }
  displayHelp = !displayHelp;
});

///////////////////
// Initial calls //
///////////////////

randomize();
drawGrid();
drawCells();
play();
